On nous habitue beaucoup trop à recopier nos leçons **sans réfléchir**. Ce n'est qu'à la fin du lycée _(et surtout à l'arrivée du secondaire - s'ajoutant au choc de la prise d'autonomie)_ que certain.e.s enseignant.e.s nous proposent une approche de **la prise de notes**.
Le problème étant que d'un autre côté on veut nous apprendre à réaliser des recherches sans plagier - à juste titre - or en terme de retranscription l'école nous apprends uniquement le plagiat.

(J'ouvre une parenthèse sur le **plagiat** : trop néfastement présent car **l'école nous apprend à retranscrire les savoirs de cette manière**, ainsi cette pratique est, pour moi, trop abolit, dans le sens où l'élève veut dès lors absolument s'approprier les informations qu'il trouve quitte à ne jamais citer des auteurs.trices et toujours reformuler leurs idées. Ne serait-ce pas là une forme de plagiat implicite ? Je cherche la petite bête simplement pour souligner qu'il est parfois intéressant de citer au lieu d'entrer dans une quête ultime de la reformulation.
Par ailleurs, plutôt que de faire du plagiat une pratique tabou, ne faudrait-il pas avant tout repenser l'apprentissage de la prise d'information dès la prise de notes en cours ? _cf suite de réflexion_)


La prise de note permet de synthétiser et de mieux comprendre ce que l'on écoute. NEANMOINS cette méthode est difficile à mettre en place et présente l'inconvénient de la perte d'information.
Toutefois pour éviter les problématiques citées ci-dessus relatives au plagiat il est nécessaire de développer le plus tôt possible la prise de note, et surtout fuir le recopiage mot pour mot dès que possible.
Dès que possible, car ce recopiage est nécessaire en primaire pour consolider la caligraphie du jeune.

La prise de notre nécessite une attention et un intérêt conséquent ==> compliqué à mettre en place
Faire en sorte de développer cette technique progressivement (synthèse de conférences, prendre en note des consignes simples à l'oral, etc). **Il est essentiel d'apprendre aux jeunes à retranscire une synthèse d'un discours oratoire.** 

Pour autant, je suis tout de même attaché à la retranscription de l'ensemble des informations pour un cours. Le **poly** dans le supérieur, _quand il est bien fait_ , est une solution intéressante : avoir une **trace écrite du support** du professeur (vraiment le support (diapo) sur lequel ce dernier se base pour former son discours), tout en nous laissant la possibilité de faire des **annotations** supplémentaires aidant à la compréhension (à l'aide de ce que l'enseignant ajoute à l'oral ou de ses réponses aux questions).
Les annotations peuvent être écrites (impression poly) ou numérique, pour cela intéressant le travail de l'IRI (Institut de Recherche et d'Innovation) : [`Lignes de Temps`](https://www.iri.centrepompidou.fr/outils/lignes-de-temps/?lang=fr_fr) _ajd obsolète_ , puis [`Polemic Tweet`](https://www.iri.centrepompidou.fr/outils/polemic-tweet/?lang=fr_fr) et enfin [`Iri Notes`](https://www.iri.centrepompidou.fr/outils/iri-notes/) (j'ai le contact du gars qui gère ce dispositif à l'IRI)
