 inviter les élèves à s’expliquer mutuellement en travaillant notamment plus en groupe.
Attention à ce que la personne qui explique n’ait pas trop de poids sur ses épaules, il faut que ce soit bénéfique pour cette dernière : expliquer à ses camarades lui permettra de clarifier d’autant plus les notions.
