Dès le plus jeune âge, avoir des matières pour apprendre à mieux se connaître, savoir ce que le jeune aime faire.
Pour cela il faudrait réfléchir à des travaux sur la personnalité du jeune : la découvrir et la développer.
L’école pourrait mêler passion et étude (ex : passionné.e d’équitation pourrait avoir un temps consacré pour se former de manière plus ou moins autonome en fonction de l’âge —> augmente curiosité et intérêt pour l’éducation)
Plus de pratique (pour aussi mieux découvrir les passions du jeune et donc le futur domaine de métier potentiel)  et plus de projection sur l’utilité de l’apprentissage (même si culture générale, btw peut-être que la culture gé actuelle = trop importante car trop de théorie)
